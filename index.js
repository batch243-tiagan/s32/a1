    let http = require('http')
    let post = 4000

    const server=http.createServer((req, res)=>{
        if(req.url=='/' && req.method=='GET'){
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end("Welcome to booking system");
        }else if(req.url=='/profile' && req.method=='GET'){
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end("Welcome to your profile");
        }else if(req.url=='/courses' && req.method=='GET'){
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end("Here's our courses Available");
        }else if(req.url=='/addCourse' && req.method=='POST'){
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end("Add course to our database");
        }
    }).listen(4000)

    console.log(`Server running at localhost: ${post}`)